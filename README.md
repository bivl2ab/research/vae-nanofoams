# VAE Nanofoams

This repository is related to the paper entitled:

"Geometrical recognition of metallic foam microstructures using a deep learning approach" (**Under revision**)



William D. Romero<sup>a</sup>, Santiago Torres-Bermudez<sup>b</sup>, Brayan Valenzuela<sup>a</sup>, Cristian C. Viáfara<sup>c</sup>, Angel M. Meléndez<sup>b</sup>, Fabio Martínez<sup>*a</sup>.

<sup>a</sup> Biomedical Imaging, Vision and Learning Laboratory [(BIVL<sup>2</sup>ab)](https://bivl2ab.uis.edu.co/), Universidad Industrial de Santander (UIS)

<sup>b</sup> Grupo de Investigaciones en Minerales, Biohidrometalurgia y Ambiente (GIMBA), Universidad Industrial de Santander (UIS)

<sup>c</sup> Grupo de Investigaciones en corrosión (GIC), Universidad Industrial de Santander (UIS)


![Graphical Absstract](Graphical_Abstract.png)

### Citation

For any work based on the proposed approach or that uses the raw videos or the same video slices, please cite the following article:

[XXXXXXXXXXXXXXXXXXXX, "Geometrical recognition of metallic foam microstructures using a deep learning approach", DOI: XXX]()

## Data 

[Request Access to Dataset]()

To download the associated imaging data: [Request Access to Dataset](). The Confocal Metallic copper-nickel nanofoams images dataset is composed by 600 nanostructured metallic copper-nickel foams (NMFs) images. 200 images per class. Each class of the dataset correspond to one NMF of 1cm<sup>2<sup>.


## Conctact Information

- William David Romero: [william.romero3@correo.uis.edu.co]()